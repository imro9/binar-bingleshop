const addItemByAdmin = require("../../controllers/addItemByAdmin");
const item = require("../../models").sequelize.model("item");

//init
describe("addItemByAdmin()", () => {
  const req = {
    body: {
      name: "some name",
      category: "some category",
      price: "some price",
      stock: "some stock",
    },
  };

  const res = {};
  res.status = jest.fn().mockReturnValue(res);
  res.json = jest.fn().mockReturnValue(res);

  const next = jest.fn();

  const mockItems = {
    create: (item.create = jest.fn()),
  };

  //init
  it("should be defined addItemByAdmin()", () => {
    expect(addItemByAdmin).toBeDefined();
  });
  //status 200
  it("should return response 201", async () => {
    mockItems.create.mockReturnValue(req);

    await addItemByAdmin(req, res, next);

    expect(res.status).toBeCalledWith(201);
    expect(res.json).toBeCalledWith({
      message: "success add some name to database",
      status: "success",
    });
  });
  //status eror
  it("should catch error", async () => {
    mockItems.create.mockRejectedValue(null);
    try {
      await addItemByAdmin(req, null, next);
    } catch (error) {
      expect(error).toBeDefined();
      expect(next).toBeCalledWith(error);
    }
  });
});
