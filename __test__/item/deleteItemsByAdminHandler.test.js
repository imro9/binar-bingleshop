const deleteItemsByAdmin = require("../../controllers/deleteItemsByAdminHandler");
const item = require("../../models").sequelize.model("item");

const req = { params: { id: 5 } };
const res = {};
res.status = jest.fn();
res.send = jest.fn();
const next = jest.fn();
const mockItem = {
  destroy: (item.destroy = jest.fn()),
  findByPk: (item.findByPk = jest.fn()),
};
const mockFoundItem = { id: 5, name: "sesuatu", category: "something", price: 4000, stock: 3 };
afterEach(() => {
  jest.restoreAllMocks();
});

describe("deleteItemsByAdmin", () => {
  it("should be defined", () => {
    expect(deleteItemsByAdmin).toBeDefined;
  });
  it("should delete items successfully", async () => {
    mockItem.findByPk.mockReturnValue(mockFoundItem);
    await deleteItemsByAdmin(req, res, next);
    //item.destroy dipanggil dengan id sesuai req
    expect(item.destroy).toBeCalledWith({ where: { id: 5 } });
    expect(res.send).toBeCalledWith("Item berhasil didelete");
  });
  it("should respond with error if item not found", async () => {
    //find item return null because item is not found
    mockItem.findByPk.mockReturnValue(null);
    await deleteItemsByAdmin(req, res, next);
    expect(res.status).toBeCalledWith(404);
    expect(res.send).toBeCalledWith("item not found");
  });
  it("should catch error and pass to next", async () => {
    mockItem.findByPk.mockReturnValue(mockFoundItem);
    mockItem.destroy.mockRejectedValue(new Error());
    await deleteItemsByAdmin(req, res, next);
    expect(next).toBeCalledWith({ message: "Unable to delete item" });
  });
});
