const findItemsByUser = require("../../controllers/findItemsByUserHandler");
const Item = require("../../models").sequelize.model("item");
const { Op } = require("sequelize");

const res = {};
res.status = jest.fn();
res.json = jest.fn();

const req = {};
const next = jest.fn();

const mockItem = {
  findAll: (Item.findAll = jest.fn()),
};

const mockItemDB = [
  { id: 1, nama: "something", category: "sesuatu" },
  { id: 2, nama: "item kedua", category: "random" },
];

afterEach(() => {
  jest.restoreAllMocks();
});

describe("findItemsByUser", () => {
  it("should be defined", () => {
    expect(findItemsByUser).toBeDefined;
  });
  it("should successfully get items", async () => {
    mockItem.findAll.mockReturnValue(mockItemDB);
    await findItemsByUser(req, res, next);
    expect(Item.findAll).toBeCalledWith({ where: { stock: { [Op.gt]: [0] } }, raw: true, attributes: ["id", "name", "category"] });
    expect(res.status).toBeCalledWith(200);
    expect(res.json).toBeCalledWith([
      { id: 1, nama: "something", category: "sesuatu" },
      { id: 2, nama: "item kedua", category: "random" },
    ]);
  });
});
