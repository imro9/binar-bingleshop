const getAdminItems = require("../../controllers/getAdminItemsHandler");
const item = require("../../models").sequelize.model("item");
const { Op } = require("sequelize");

const res = {};
res.send = jest.fn().mockReturnValue(res);
res.json = jest.fn().mockReturnValue(res);
res.status = jest.fn().mockReturnValue(res);

const next = jest.fn();

const mockItem = {
  findOne: (item.findOne = jest.fn()),
  findAll: (item.findAll = jest.fn()),
};
const mockItemDB = [
  { id: 2, name: "blus", category: "baju", stock: 3, price: 50000 },
  { id: 4, name: "sneaker", category: "sepatu", stock: 2, price: 78000 },
];

afterEach(() => {
  jest.restoreAllMocks();
});
describe("getAdminItems()", () => {
  let mockRequest;
  beforeEach(() => {
    mockRequest = {
      query: {},
    };
  });
  it("should be defined", () => {
    expect(getAdminItems).toBeDefined;
  });
  it("should successfully retrieve items", async () => {
    const req = mockRequest;
    req.query = {
      maxharga: 1000000,
      minharga: 500,
      maxqty: 30,
      minqty: 2,
    };
    mockItem.findAll.mockReturnValue(mockItemDB);
    await getAdminItems(req, res, next);
    //parameter item.findAll sesuai dengan req.query
    expect(item.findAll).toBeCalledWith({
      where: {
        price: {
          [Op.between]: [500, 1000000],
        },
        stock: {
          [Op.between]: [2, 30],
        },
      },
    });
    //res.send mengirimkan mockItemDB
    expect(res.send).toBeCalledWith({
      status: "success",
      data: [
        { id: 2, name: "blus", category: "baju", stock: 3, price: 50000 },
        { id: 4, name: "sneaker", category: "sepatu", stock: 2, price: 78000 },
      ],
    });
  });
  it("should catch error and pass to next if failed to get items from db", async () => {
    const req = mockRequest;
    req.query = {
      maxharga: 1000000,
      minharga: 500,
      maxqty: 30,
      minqty: 2,
    };
    //error ketika query item.findAll gagal
    mockItem.findAll.mockRejectedValue(new Error());
    await getAdminItems(req, res, next);
    expect(next).toBeCalled;
    expect(next).toBeCalledWith({ message: "server error, items not found" });
  });
  it("should reassign 0 for minqty and minharga if not inputed by user", async () => {
    //minqty dan minharga tidak ada
    const req = mockRequest;
    req.query = {
      maxharga: 300000,
      maxqty: 10,
    };
    mockItem.findAll.mockReturnValue(mockItemDB);
    await getAdminItems(req, res, next);
    //parameter item.findAll menggunakan 0 untuk minharga dan minqty
    expect(item.findAll).toBeCalledWith({
      where: {
        price: {
          [Op.between]: [0, 300000],
        },
        stock: {
          [Op.between]: [0, 10],
        },
      },
    });
  });
  it("should query database for maxharga if not input by user", async () => {
    //maxharga tidak ada
    const req = mockRequest;
    req.query = {
      minharga: 10,
      minqty: 20,
      maxqty: 100,
    };
    //query item termahal
    mockItem.findOne.mockReturnValue({ id: 10, nama: "sesuatu", category: "something", stock: 30, price: 1000 });
    mockItem.findAll.mockReturnValue(mockItemDB);
    await getAdminItems(req, res, next);
    //parameter item.findAll maxharga sesuai mock return value item.findOne
    expect(item.findAll).toBeCalledWith({
      where: {
        price: {
          [Op.between]: [10, 1000],
        },
        stock: {
          [Op.between]: [20, 100],
        },
      },
    });
  });
  it("should pass error to next if failed to query database for maxharga", async () => {
    const req = mockRequest;
    req.query = {
      minharga: 10,
      minqty: 20,
      maxqty: 100,
    };
    //gagal query
    mockItem.findOne.mockImplementation(() => {
      throw new Error();
    });
    await getAdminItems(req, res, next);
    expect(next).toBeCalledWith({ message: "server error, mohon masukkan parameter query" });
  });
  it("should query database for maxqty if not input by user", async () => {
    const req = mockRequest;
    req.query = {
      minharga: 10,
      minqty: 20,
      maxharga: 100,
    };
    mockItem.findOne.mockReturnValue({ nama: "sesuatu", category: "something", stock: 30, price: 1000 });
    mockItem.findAll.mockReturnValue(mockItemDB);
    await getAdminItems(req, res, next);
    //parameter item.findAll maxqty sesuai dgn mockreturnvalue item.findOne
    expect(item.findAll).toBeCalledWith({
      where: {
        price: {
          [Op.between]: [10, 100],
        },
        stock: {
          [Op.between]: [20, 30],
        },
      },
    });
  });
  it("should pass error to next if failed to query database for maxqty", async () => {
    const req = mockRequest;
    req.query = {
      minharga: 10,
      minqty: 20,
      maxharga: 100,
    };
    mockItem.findOne.mockImplementation(() => {
      throw new Error();
    });
    await getAdminItems(req, res, next);
    expect(next).toBeCalledWith({ message: "server error, mohon masukkan parameter query" });
  });
  it("should return error and pass to next if query param isNaN", async () => {
    //req.query bukan angka
    const req = mockRequest;
    req.query = {
      maxharga: "string",
      minharga: "not a number",
      maxqty: "invalid",
      minqty: "not a number",
    };
    await getAdminItems(req, res, next);
    expect(res.status).toBeCalledWith(400);
    expect(res.send).toBeCalledWith("invalid query parameter");
  });
  it("should return error and pass to next if maxharga and maxqty less than minharga and minqty", async () => {
    //maxharga < minharga, maxqty < minqty
    const req = mockRequest;
    req.query = {
      maxharga: 10,
      minharga: 100,
      maxqty: 2,
      minqty: 10,
    };
    await getAdminItems(req, res, next);
    expect(res.status).toBeCalledWith(400);
    expect(res.send).toBeCalledWith("invalid query parameter");
  });
});
