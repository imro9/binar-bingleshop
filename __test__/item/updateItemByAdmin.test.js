const updateItemByAdmin = require("../../controllers/updateItemByAdmin");
const item = require("../../models").sequelize.model("item");

describe("updateItemByAdmin()", () => {
  const req = {
    params: {
      id: 5, //items id
    },
    body: {
      name: "Some Name",
      category: "Some category",
      price: "Some price",
      stock: "Some stock",
    },
  };
  const res = {};
  res.status = jest.fn().mockReturnValue(res);
  res.json = jest.fn().mockReturnValue(res);

  const next = jest.fn();

  const mockItem = {
    findOne: (item.findOne = jest.fn()),
    update: (item.update = jest.fn()),
  };

  it("should be defined updateItemByAdmin()", () => {
    expect(updateItemByAdmin).toBeDefined();
  });

  it("should be error when id is not number", async () => {
    const mockRequestParam = {
      ...req,
      params: {
        id: "x",
      },
    };
    await updateItemByAdmin(mockRequestParam, res, next);
    expect(next).toBeCalledWith({ message: "item id should be a number" });
  });

  it("should be error when order is empty", async () => {
    mockItem.findOne.mockReturnValue(null);
    await updateItemByAdmin(req, res, next);
    expect(next).toBeCalledWith({ message: "item not found" });
  });

  it("should return response 201", async () => {
    mockItem.findOne.mockReturnValue(req);
    await updateItemByAdmin(req, res, next);
    expect(res.status).toBeCalledWith(201);
    expect(res.json).toBeCalledWith({ status: "success", message: `item data with id ${req.params.id} updated` });
  });

  it("should catch error", async () => {
    mockItem.findOne.mockRejectedValue(null);
    try {
      await updateItemByAdmin(req, null, next);
    } catch (error) {
      expect(error).toBeDefined();
      expect(next).toBeCalledWith(error);
    }
  });
});
