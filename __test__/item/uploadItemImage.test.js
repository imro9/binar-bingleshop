const { updateItem, itemImageValidator } = require("../../controllers/uploadItemImage");
const item = require("../../models").sequelize.model("item");
const uploadCloudinary = require("../../libs/upload_cloudinary");
const acceptedFile = ["image/jpg", "image/jpeg", "image/png"];
const { Blob } = require("buffer");
const fs = require("fs");

class MockFile {
  create(name, size, mimetype) {
    name = name || "mock.txt";
    size = size || 1024;
    mimetype = mimetype || "plain/txt";

    function range(count) {
      var output = "";
      for (var i = 0; i < count; i++) {
        output += "a";
      }
      return output;
    }

    var blob = new Blob([range(size)], { type: mimetype });
    Object.assign(blob, {
      lastModifiedDate: new Date(),
      name: name,
      path: __dirname + "/temp/" + name,
      mimetype,
    });

    return blob;
  }
}

describe("Item Controller", () => {
  describe("UpdateItem()", () => {
    const mockUploader = {
      upload: (uploadCloudinary.upload = jest.fn()),
    };
    const mockUploadResult = {
      secure_url: "https://cloudinary/result.jpg",
    };

    const res = {};
    res.status = jest.fn().mockReturnValue(res);
    res.sendStatus = jest.fn().mockReturnValue(res);

    afterAll(() => {
      jest.restoreAllMocks();
    });
    it("should be define", () => {
      expect(updateItem).toBeDefined();
    });

    it("should success upload", async () => {
      const size = Math.pow(2, 20);
      const mockFile = new MockFile();
      const file = mockFile.create("picture.jpg", size, "image/jpeg");

      const next = jest.fn();

      const req = {
        params: {
          id: 1,
        },
        file: file,
      };

      const mockItem = {
        update: (item.update = jest.fn()),
      };
      mockUploader.upload.mockResolvedValue(mockUploadResult);

      await updateItem(req, res, next);
      expect(mockUploader.upload).toBeCalledWith(req.file.path);
      expect(mockItem.update).toBeCalledWith(
        {
          img_url: mockUploadResult.secure_url,
        },
        {
          where: { id: req.params.id },
        }
      );
      expect(res.sendStatus).toBeCalledWith(204);
    });

    it("should error when upload failed", async () => {
      const size = Math.pow(2, 20);
      const mockFile = new MockFile();
      const file = mockFile.create("picture.jpg", size, "image/jpeg");

      const next = jest.fn();

      const req = {
        params: {
          id: 1,
        },
        file: file,
      };

      mockUploader.upload.mockRejectedValue("Error upload image");

      await updateItem(req, res, next);
      expect(mockUploader.upload).toBeCalledWith(req.file.path);
      expect(next).toBeCalledWith({ status: 500, message: "Error upload image" });
    });
  });

  describe("itemImageValidator()", () => {
    it("should have callback error when file type is not supported", () => {
      const mockFile = new MockFile();
      const file = mockFile.create();

      const callback = jest.fn();

      itemImageValidator(null, file, callback);
      expect(callback).toBeCalledWith(new Error("File must be .jpg|jpeg|png type"));
    });

    it("should success validate image type", () => {
      const size = Math.pow(2, 20);
      const mockFile = new MockFile();
      const file = mockFile.create("picture.jpg", size, "image/jpeg");

      const callback = jest.fn();

      itemImageValidator(null, file, callback);
      expect(callback).toBeCalledWith(null, true);
    });
  });
});
