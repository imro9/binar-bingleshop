const authorization = require("../../middelware/authorization");

const mockRequest = ({ header = {}, user = {} }) => {
  return {
    header,
    user,
  };
};

const mockResponse = () => {
  const res = {};
  res.status = jest.fn().mockReturnValue(res);
  res.json = jest.fn().mockReturnValue(res);
  return res;
};

const mockNext = () => {
  return jest.fn();
};

describe("Authorization unit test", () => {
  it("should be defined", () => {
    authorization();
    expect(authorization).toBeDefined();
  });

  it("should return 403 when user have wrong access", () => {
    const req = mockRequest({ user: { role: "admin" } });
    const res = mockResponse();
    const next = mockNext();
    const auth = authorization(["costumer"]);
    auth[1](req, res, next);
    expect(res.status).toBeCalledWith(403);
    expect(res.json).toBeCalledWith({ status: "fail", message: "You dont have access to this path!" });
  });

  it("should pass middleware when have right access", () => {
    const req = mockRequest({ user: { role: "costumer" } });
    const res = mockResponse();
    const next = mockNext();
    const auth = authorization(["costumer"]);
    auth[1](req, res, next);
    expect(next).toBeCalled();
  });
});
