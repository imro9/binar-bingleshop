const createOrderValidation = require("../../middelware/createOrderValidation");
const Validator = require("fastest-validator");
const v = new Validator();

const mockRequest = (body = {}, params = {}, query = {}) => {
  return {
    body,
    params,
    query,
  };
};

const mockResponse = () => {
  req = {};
  req.status = jest.fn().mockReturnValue(req);
  req.json = jest.fn().mockReturnValue(req);
  return req;
};

const mockNext = () => {
  const next = jest.fn();
  return next;
};

const mockValidator = () => {
  const validator = {};
  validator.compile = v.compile = jest.fn();
  return validator;
};

afterAll(() => {
  jest.restoreAllMocks();
});

describe("create order validation test", () => {
  it("should be defined", () => {
    expect(createOrderValidation).toBeDefined();
  });

  it("should be rejected when receive wrong data format", () => {
    const req = mockRequest({ data: 1 });
    const res = mockResponse();
    const next = mockNext();
    const validation = mockValidator();
    const returnedValue = [
      {
        type: "required",
        message: "The 'data_item' field is required.",
        field: "data_item",
      },
    ];
    validation.compile.mockReturnValue(returnedValue);
    createOrderValidation(req, res, next);
    expect(res.status).toBeCalledWith(400);
    expect(res.json).toBeCalledWith(returnedValue);
  });

  it("should be rejected when there is property missing missing", () => {
    const req = mockRequest({ data_item: [{ item_id: 1 }] });
    const res = mockResponse();
    const next = mockNext();
    const validation = mockValidator();
    const returnedValue = [
      {
        type: "required",
        message: "The 'data_item[0].quantity' field is required.",
        field: "data_item[0].quantity",
      },
    ];
    validation.compile.mockReturnValue(returnedValue);
    createOrderValidation(req, res, next);
    expect(res.status).toBeCalledWith(400);
    expect(res.json).toBeCalledWith(returnedValue);
  });

  it("should be pass the middleware when data correct", () => {
    const req = mockRequest({ data_item: [{ item_id: 1, quantity: 1 }] });
    const res = mockResponse();
    const next = mockNext();
    const validation = mockValidator();
    validation.compile.mockReturnValue(true);
    createOrderValidation(req, res, next);
    expect(next).toBeCalled();
  });
});
