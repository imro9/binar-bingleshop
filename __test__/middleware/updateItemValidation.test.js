const updateItemValidation = require("../../middelware/updateItemValidation");

describe("updateItemValidation()", () => {
  const res = {};
  res.status = jest.fn().mockReturnValue(res);
  res.json = jest.fn().mockReturnValue(res);

  const next = jest.fn();

  it("should be defined updateItemValidation()", () => {
    expect(updateItemValidation).toBeDefined();
  });

  it("should be error with status code 400", async () => {
    const RegEror = {
      body: {
        name: "",
        category: "",
        price: "x",
        stock: "x",
      },
    };
    await updateItemValidation(RegEror, res, next);
    expect(res.status).toBeCalledWith(400);
    expect(res.json).toBeCalledWith({
      status: "fail",
      message: "incompatible data field sent",
    });
  });

  it("should be Next", () => {
    const reqValid = {
      body: {
        name: "Some Name",
        category: "Some category",
        price: 1000,
        stock: 1,
      },
    };
    updateItemValidation(reqValid, res, next);
    expect(next).toBeCalled();
  });
});
