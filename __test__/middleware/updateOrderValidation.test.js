const updateOrderValidation = require("../../middelware/updateOrderValidation");
const Validator = require("fastest-validator");
const v = new Validator();

describe("updateOrderValidation()", () => {
  const res = {};
  res.status = jest.fn().mockReturnValue(res);
  res.json = jest.fn().mockReturnValue(res);

  const next = jest.fn();

  const validator = {};
  validator.compile = v.compile = jest.fn();

  it("should be defined udpateOrderValidation()", () => {
    expect(updateOrderValidation).toBeDefined();
  });

  it("should be error when form validation is invalid", () => {
    const req = {
      body: {
        status: "pending",
      },
    };

    updateOrderValidation(req, res, next);
    expect(res.status).toBeCalledWith(400);
    expect(res.json).toBeCalledWith({
      status: "fail",
      message: "incompatible data field sent",
    });
  });

  it("should be pass the middleware when data correct", () => {
    const req = {
      body: {
        status: "completed",
      },
    };
    validator.compile.mockReturnValue(true);
    updateOrderValidation(req, res, next);
    expect(next).toBeCalled();
  });
});
