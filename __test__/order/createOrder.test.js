const createOrder = require("../../controllers/createOrder");
const Order = require("../../models").sequelize.model("order");
const Item = require("../../models").sequelize.model("item");
const Order_detail = require("../../models").sequelize.model("order_detail");

const mockRequest = (body = [], user = {}, params = {}, query = {}) => {
  return {
    body,
    user,
    params,
    query,
  };
};

const mockResponse = () => {
  const res = {};
  res.status = jest.fn().mockReturnValue(res);
  res.json = jest.fn().mockReturnValue(res);
  return res;
};

const mockDatabase = () => {
  const mockDb = {};
  mockDb.Item = {
    findAll: (Item.findAll = jest.fn()),
    increment: (Item.increment = jest.fn()),
  };
  mockDb.Order = {
    create: (Order.create = jest.fn()),
  };
  mockDb.Order_detail = {
    bulkCreate: (Order_detail.bulkCreate = jest.fn()),
  };
  return mockDb;
};

const mockNext = () => {
  const next = jest.fn();
  return next;
};

afterEach(() => {
  jest.restoreAllMocks();
});

describe("create order end point test", () => {
  it("sholud be defined", () => {
    expect(createOrder).toBeDefined();
  });

  it("should return error when cannot find data item in database", async () => {
    const req = mockRequest(
      {
        data_item: [
          { item_id: 20, quantity: 1 },
          { item_id: 1, quantity: 1 },
        ],
      },
      { user_id: 1 }
    );
    const res = mockResponse();
    const next = mockNext();
    const mockDB = mockDatabase();
    mockDB.Item.findAll.mockReturnValue([{ id: 1, price: 5000, stock: 10 }]);
    await createOrder(req, res, next);
    expect(res.status).toBeCalledWith(400);
    expect(res.json).toBeCalledWith({ status: "fail", message: "cannot find selected item in our database!" });
  });

  it("should success when data valid", async () => {
    const req = mockRequest({ data_item: [{ item_id: 1, quantity: 1 }] }, { user_id: 1 });
    const res = mockResponse();
    const next = mockNext();
    const mockDB = mockDatabase();
    mockDB.Item.findAll.mockReturnValue([{ id: 1, price: 5000, stock: 10 }]);
    mockDB.Order.create.mockReturnValue({ id: 2 });
    mockDB.Order_detail.bulkCreate.mockImplementation(() => {});
    await createOrder(req, res, next);
    expect(res.status).toBeCalledWith(201);
    expect(res.json).toBeCalledWith({ status: "success", message: `new order created with id ${2}` });
  });

  it("should throw error", async () => {
    const req = mockRequest({ data_item: [{ item_id: 1, quantity: 1 }] }, { user_id: 1 });
    const res = mockResponse();
    const next = mockNext();
    const mockDB = mockDatabase();
    mockDB.Item.findAll.mockRejectedValue(new Error("Some database error"));
    await createOrder(req, res, next);
    expect(next).toBeCalled();
  });
});
