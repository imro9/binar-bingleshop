const deleteOrderByCustomer = require("../../controllers/deleteOrderByCustomer");
const Order = require("../../models").sequelize.model("order");

const res = {};
res.json = jest.fn().mockReturnValue(res);
res.status = jest.fn().mockReturnValue(res);
const next = jest.fn();

const mockOrder = {
  findOne: (Order.findOne = jest.fn()),
  update: (Order.update = jest.fn()),
};
afterEach(() => {
  jest.restoreAllMocks();
});
describe("deleteOrderbycustomer", () => {
  let mockRequest;
  beforeEach(() => {
    mockRequest = {
      params: { id: "1" },
      user: 2,
    };
  });
  it("should be defined", () => {
    expect(deleteOrderByCustomer).toBeDefined;
  });
  it("should fail to find order", async () => {
    const req = mockRequest;
    mockOrder.findOne.mockReturnValue(null);
    await deleteOrderByCustomer(req, res, next);
    expect(Order.findOne).toBeCalledWith({ raw: true, where: { id: 1 } });
    expect(res.status).toBeCalledWith(400);
    expect(res.json).toBeCalledWith({ status: "fail", message: `Didn't find the order on ID 1` });
    expect(Order.update).not.toBeCalled;
  });
  it("should fail if user id does not match user who ordered", async () => {
    const req = mockRequest;
    mockOrder.findOne.mockReturnValue({ user_id: 9 });
    await deleteOrderByCustomer(req, res, next);
    expect(res.status).toBeCalledWith(404);
    expect(res.json).toBeCalledWith({ status: "fail", message: "you dont have access to this resource" });
    expect(Order.update).not.toBeCalled;
  });
  it("should fail if order is already complete", async () => {
    const req = mockRequest;
    mockOrder.findOne.mockReturnValue({ status: "completed" });
    await deleteOrderByCustomer(req, res, next);
    expect(res.status).toBeCalledWith(409);
    expect(res.json).toBeCalledWith({ status: "fail", message: "Cannot cancel order, because it's already completed!" });
    expect(Order.update).not.toBeCalled;
  });
  it("should fail if order is already canceled", async () => {
    const req = mockRequest;
    mockOrder.findOne.mockReturnValue({ status: "canceled" });
    await deleteOrderByCustomer(req, res, next);
    expect(res.status).toBeCalledWith(409);
    expect(res.json).toBeCalledWith({ status: "fail", message: `Orders ID 1 already canceled` });
    expect(Order.update).not.toBeCalled;
  });
  it("should successfully cancel order", async () => {
    const req = mockRequest;
    mockOrder.findOne.mockReturnValue({ status: "ongoing" });
    await deleteOrderByCustomer(req, res, next);
    expect(res.status).toBeCalledWith(201);
    expect(Order.update).toBeCalledWith(
      { status: "canceled" },
      {
        where: { id: 1 },
      }
    );
  });
});
