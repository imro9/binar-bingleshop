const findOrdersByUser = require("../../controllers/findOrdersByUser");
const Order = require("../../models").sequelize.model("order");

const res = {};
res.status = jest.fn();
res.json = jest.fn();

const mockRequest = (body = [], user = {}, params = {}, query = {}) => {
  return {
    body,
    user,
    params,
    query,
  };
};

const next = jest.fn();

const mockItem = {
  findAll: (Order.findAll = jest.fn()),
};

const mockOrderDB = [{ id: 1, status: "processed", total: 30000 }];

afterEach(() => {
  jest.restoreAllMocks();
});

describe("findItemsByUser", () => {
  it("should be defined", () => {
    expect(findOrdersByUser).toBeDefined;
  });

  it("should return zero order data", async () => {
    const req = mockRequest([], { user_id: 2 });

    mockItem.findAll.mockReturnValue([]);
    await findOrdersByUser(req, res, next);

    expect(Order.findAll).toBeCalledWith({ where: { user_id: 2 }, raw: true, attributes: ["id", "status", "total"] });
    expect(res.status).toBeCalledWith(200);
    expect(res.json).toBeCalledWith({ status: "fail", message: "no order yet!" });
  });

  it("should successfully get data order", async () => {
    const req = mockRequest([], { user_id: 1 });

    mockItem.findAll.mockReturnValue(mockOrderDB);
    await findOrdersByUser(req, res, next);

    expect(Order.findAll).toBeCalledWith({ where: { user_id: 1 }, raw: true, attributes: ["id", "status", "total"] });
    expect(res.status).toBeCalledWith(200);
    expect(res.json).toBeCalledWith({ status: "succes", data: [{ id: 1, status: "processed", total: 30000 }] });
  });
});
