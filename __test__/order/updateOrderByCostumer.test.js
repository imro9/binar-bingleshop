const updateOrderByCustomer = require("../../controllers/updateOrderByCostumer");
const order = require("../../models").sequelize.model("order");

describe("updateOrderByCustomer()", () => {
  const req = {
    user: {
      user_id: 1,
    },
    params: {
      id: 5, //order id
    },
    body: {
      status: "completed",
    },
  };

  const res = {};
  res.status = jest.fn().mockReturnValue(res);
  res.json = jest.fn().mockReturnValue(res);

  const next = jest.fn();

  const mockOrder = {
    findOne: (order.findOne = jest.fn()),
    update: (order.update = jest.fn()),
  };

  it("should be defined updateOrderByCustomer()", () => {
    expect(updateOrderByCustomer).toBeDefined();
  });

  it("should be error when id is not number", async () => {
    const mockRequestParam = {
      ...req,
      params: {
        id: "x",
      },
    };

    await updateOrderByCustomer(mockRequestParam, res, next);

    expect(next).toBeCalledWith({ message: "order id should be a number" });
  });

  it("should return error with status code 404, because user is not match ", async () => {
    //given that user is is not match with order database
    mockOrder.findOne.mockReturnValue({ user_id: 10 });

    await updateOrderByCustomer(req, res, next);

    expect(res.status).toBeCalledWith(404);
    expect(res.json).toBeCalledWith({
      status: "fail",
      message: "you dont have access to this resource",
    });
  });

  it("should be error when order is empty", async () => {
    mockOrder.findOne.mockReturnValue(null);
    await updateOrderByCustomer(req, res, next);
    expect(next).toBeCalledWith({ message: "order not found" });
  });

  it("should be error with status code 400, because the status order is already complete", async () => {
    mockOrder.findOne.mockReturnValue({ status: "completed", user_id: 1 });
    await updateOrderByCustomer(req, res, next);
    expect(res.status).toBeCalledWith(400);
    expect(res.json).toBeCalledWith({
      status: "fail",
      message: "order already completed, nothing changed",
    });
  });

  it("should be successfull when updating order status to complete", async () => {
    mockOrder.findOne.mockReturnValue({ status: "pending", user_id: 1 });
    await updateOrderByCustomer(req, res, next);
    expect(res.status).toBeCalledWith(201);
    expect(res.json).toBeCalledWith({
      status: "success",
      message: `data order with id ${req.params.id} updated`,
    });
  });

  it("should catch error", async () => {
    mockOrder.findOne.mockRejectedValue(null);
    try {
      await updateOrderByCustomer(req, null, next);
    } catch (error) {
      expect(error).toBeDefined();
      expect(next).toBeCalledWith(error);
    }
  });
});
