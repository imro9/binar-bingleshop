const userLogin = require("../../controllers/userLoginHandler");
const { sequelize } = require("../../models");
const user = sequelize.model("user");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

const res = {
  json: jest.fn(),
  send: jest.fn(),
  status: jest.fn(),
};
const next = jest.fn();

const mockUser = {
  findOne: (user.findOne = jest.fn()),
};
const mockJWT = {
  sign: (jwt.sign = jest.fn()),
};
const mockBcrypt = {
  compareSync: (bcrypt.compareSync = jest.fn()),
};
const mockFoundUser = { id: 3, email: "something", password: "hashed password", role: "admin", is_verified: true };

afterEach(() => {
  jest.restoreAllMocks();
});

describe("userLoginHandler", () => {
  let mockReq;
  beforeEach(() => {
    mockReq = {
      body: {},
    };
  });
  it("should be defined", () => {
    expect(userLogin).toBeDefined;
  });
  it("should successfully login", async () => {
    const req = mockReq;
    req.body = { email: "something", password: "sesuatu" };
    mockUser.findOne.mockReturnValue(mockFoundUser);
    mockBcrypt.compareSync.mockReturnValue(true);
    mockJWT.sign.mockReturnValue("some token");
    await userLogin(req, res, next);
    expect(jwt.sign).toBeCalledWith(
      { user_id: 3, role: "admin" },
      "9ds3w5V6Jk2E0TDCgkJCsNl7Cjw7K85ZmMmobdU7i9w87uTYgaDONFp3oNsXeBAySwv3utZkbW3PuT06jezJ8g8oXBj3CIrmCGan"
    );
    expect(res.status).toBeCalledWith(200);
    expect(res.json).toBeCalledWith({ message: "Login Success", token: "Bearer some token" });
    expect(next).not.toBeCalled;
  });

  it("should fail login because user not found", async () => {
    const req = mockReq;
    req.body = { email: "something", password: "sesuatu" };
    mockUser.findOne.mockReturnValue(null);
    await userLogin(req, res, next);
    expect(res.status).toBeCalledWith(400);
    expect(res.send).toBeCalledWith("User not found or not registered!");
    expect(next).toBeCalledWith({ message: "user not found" });
  });

  it("should fail login because password is wrong", async () => {
    const req = mockReq;
    req.body = { email: "something", password: "wrong password" };
    mockUser.findOne.mockReturnValue(mockFoundUser);
    mockBcrypt.compareSync.mockReturnValue(false);
    await userLogin(req, res, next);
    expect(res.status).toBeCalledWith(401);
    expect(res.json).toBeCalledWith({ status: "fail", message: "Password wrong!" });
    expect(next).toBeCalledWith({ message: "wrong password" });
  });

  it("should fail login because the account is not verified", async () => {
    const req = mockReq;
    req.body = { email: "something", password: "wrong password" };
    mockUser.findOne.mockReturnValue({ ...mockFoundUser, is_verified: false });
    mockBcrypt.compareSync.mockReturnValue(true);
    await userLogin(req, res, next);

    expect(res.status).toBeCalledWith(400);
    expect(res.json).toBeCalledWith({ message: "account is not verified, please check your email!" });
    expect(next).toBeCalledWith({ message: "account is not verified" });
  });
});
