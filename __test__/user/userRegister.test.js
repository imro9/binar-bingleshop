const userRegister = require("../../controllers/userRegister");
const User = require("../../models").sequelize.model("user");
const bcrypt = require("bcrypt");

describe("userRegister()", () => {
  const req = {
    body: {
      name: "some name",
      email: "some email",
      password: "some password",
    },
  };

  const res = {};
  res.status = jest.fn().mockReturnValue(res);
  res.json = jest.fn().mockReturnValue(res);

  const next = jest.fn();

  const mockUser = {
    findOne: (User.findOne = jest.fn()),
    create: (User.create = jest.fn()),
  };

  it("should be defined userRegister()", () => {
    expect(userRegister).toBeDefined();
  });

  it("should catch error", async () => {
    mockUser.findOne.mockRejectedValue(null);
    try {
      await userRegister(req, null, next);
    } catch (error) {
      expect(error).toBeDefined();
      expect(next).toBeCalledWith(error);
    }
  });

  it("should return response 400 with error message user exist", async () => {
    mockUser.findOne.mockReturnValue(req.body);

    await userRegister(req, res, next);

    expect(res.status).toBeCalledWith(400);
    expect(res.json).toBeCalledWith({
      status: "fail",
      message: "email has been registered!",
    });
  });

  it("should return response 200", async () => {
    const mockBcrypt = {
      genSaltSync: (bcrypt.genSaltSync = jest.fn()),
      hashSync: (bcrypt.hashSync = jest.fn()),
    };

    mockUser.findOne.mockReturnValue(null);
    mockBcrypt.genSaltSync.mockReturnValue("salted");
    mockBcrypt.hashSync.mockReturnValue("hashed password");

    await userRegister(req, res, next);

    expect(res.json).toBeCalledWith({
      status: "success",
      message: "user successfully signed up",
    });
  });
});
