const item = require("../models").sequelize.model("item");

module.exports = async (req, res,next) => {
  const { name, category, price, stock } = req.body;
  try {
    await item.create({ name, category, price, stock });
    return res.status(201).json({ status: "success", message: `success add ${name} to database` });
  } catch (error) {
    next(error);
  }
};
