const Order = require("../models").sequelize.model("order");
const Item = require("../models").sequelize.model("item");
const Order_detail = require("../models").sequelize.model("order_detail");

module.exports = async (req, res, next) => {
  const { user_id } = req.user;
  const { data_item } = req.body;
  try {
    const idItems = data_item.map((item) => item["item_id"]);
    const items = await Item.findAll({
      where: { id: idItems },
      raw: true,
      attributes: ["id", "price", "stock"],
    });
    if (data_item.length != items.length) {
      return res.status(400).json({ status: "fail", message: "cannot find selected item in our database!" });
    }
    const mergedData = []
    items.forEach((item) => {
      mergedData.push({...item,...(data_item.find(order => item.id === order.item_id))})
    });
    const totalOrder = mergedData.reduce((a, b) => a + b["price"] * b["quantity"], 0);
    const new_order = await Order.create({ user_id, total: totalOrder });
    const orderDetailInput = mergedData.map((item) => {
      const container = {};
      container.order_id = new_order.id;
      container.item_id = item.id;
      container.quantity = item.quantity;
      container.sub_total = item.price;
      return container;
    });
    Order_detail.bulkCreate(orderDetailInput, { returning: false });
    orderDetailInput.forEach((order) => {
      Item.increment({ stock: -order.quantity }, { where: { id: order.item_id } });
    });
    return res.status(201).json({ status: "success", message: `new order created with id ${new_order.id}` });
  } catch (error) {
    next(error);
  }
};
