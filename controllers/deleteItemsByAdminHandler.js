const { sequelize } = require("../models");
const item = sequelize.model("item");

module.exports = async (req, res, next) => {
  const deletedID = req.params.id;
  try {
    const foundItem = await item.findByPk(deletedID);
    if (foundItem === null) {
      res.status(404)
      res.send("item not found");
      return
    }
    else {await item.destroy({ where: { id: deletedID } });
    res.send("Item berhasil didelete");}
  }
    catch (error) {
    console.log(error);
    next({ message: "Unable to delete item" });
  }
};
