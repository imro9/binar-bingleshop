const { order: Order } = require("../models");

module.exports = async (req, res, next) => {
  try {
    const id = parseInt(req.params.id);
    const order = await Order.findOne({ raw: true, where: { id } });
    const { user_id } = req.user;

    if (order === null) {
      return res.status(400).json({ status: "fail", message: `Didn't find the order on ID ${id}` });
    }
    if (order.user_id !== user_id) {
      return res.status(404).json({ status: "fail", message: "you dont have access to this resource" });
    }
    if (order.status === "completed") {
      return res.status(409).json({ status: "fail", message: "Cannot cancel order, because it's already completed!" });
    }
    if (order.status === "canceled") {
      return res.status(409).json({ status: "fail", message: `Orders ID ${id} already canceled` });
    }
    Order.update(
      {
        status: "canceled",
      },
      {
        where: { id },
      }
    );

    res.status(201).json({ status: "success", message: `Order with id ${id} has been canceled` });
  } catch (error) {
    next(error);
  }
};
