const Item = require("../models").sequelize.model("item")
const { Op } = require("sequelize");
module.exports = async (req, res, next) => {
  const items = await Item.findAll({ where: { stock: { [Op.gt]: [0] } }, raw: true, attributes: ["id", "name", "category"] });
  res.status(200)
  return res.json(items);
};
