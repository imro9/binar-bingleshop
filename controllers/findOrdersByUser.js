const Order = require("../models").sequelize.model("order");

module.exports = async (req, res, next) => {
  const { user_id } = req.user;

  const orders = await Order.findAll({ where: { user_id }, raw: true, attributes: ["id", "status", "total"] });

  if (orders.length === 0) {
    res.status(200);
    res.json({ status: "fail", message: "no order yet!" });
    return;
  }

  res.status(200);
  res.json({ status: "succes", data: orders });
  return;
};
