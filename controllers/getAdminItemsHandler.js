const { Op } = require("sequelize");

const { sequelize } = require("../models");
const item = sequelize.model("item");

module.exports = async (req, res, next) => {
  //parameter search filter dalam query
  var { maxqty, minqty, maxharga, minharga } = req.query;
  //bila parameter query tidak ada
  if (!minqty) {
    minqty = 0;
  }
  if (!minharga) {
    minharga = 0;
  }
  if (!maxqty) {
    try {
      const terbanyak = await item.findOne({ order: [["stock", "DESC"]] });
      maxqty = terbanyak.stock;
    } catch (error) {
      console.log(error);
      next({ message: "server error, mohon masukkan parameter query" });
    }
  }
  if (!maxharga) {
    try {
      const termahal = await item.findOne({ order: [["price", "DESC"]] });
      maxharga = termahal.price;
    } catch (error) {
      console.log(error);
      next({ message: "server error, mohon masukkan parameter query" });
    }
  }
  //memastikan tipe data query adalah angka
  if (minqty >= maxqty || minharga >= maxharga) {
    return res.status(400).send("invalid query parameter");
  }
  try {
    const foundItems = await item.findAll({
      where: {
        price: {
          [Op.between]: [minharga, maxharga],
        },
        stock: {
          [Op.between]: [minqty, maxqty],
        },
      },
    });
    return res.send({ status: "success", data: foundItems });
  } catch (error) {
    console.log(error);
    next({ message: "server error, items not found" });
  }
};
