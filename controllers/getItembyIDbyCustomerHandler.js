const { sequelize } = require("../models");
const item = sequelize.model("item");

module.exports = async (req, res, next) => {
  const findItemId = req.params.id;
  try {
    const foundItem = await item.findByPk(findItemId);
    if (foundItem === null) {
      return res.status(404).send("item not found");
    } else if (foundItem.stock == 0) {
      return res.send("item not ready stock");
    } else {
      res.json(foundItem);
    }
  } catch (error) {
    console.log(error);
    next({ message: "server error, can't find item" });
  }
};
