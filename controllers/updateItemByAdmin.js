const { item: Item } = require("../models");

module.exports = async (req, res, next) => {
  try {
    const { id } = req.params;
    if (isNaN(parseInt(id))) {
      return next({ message: 'item id should be a number' });
    }
    const item = await Item.findOne({ where: { id: Number(id) }, raw: true });
    if (item === null) {
      return next({ message: 'item not found' });
    }
    const { name, category, price, stock } = req.body;
    const updated_item = { name: name || item.name, category: category || item.category, price: price || item.price, stock: stock || item.stock };
    Item.update({ name: updated_item.name, category: updated_item.category, price: updated_item.category, price: updated_item.price, stock: updated_item.stock }, { where: { id: +id } });
    return res.status(201).json({ status: "success", message: `item data with id ${id} updated` });
  } catch (error) {
    next(error);
  }
};
