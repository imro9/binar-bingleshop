const { order: Order } = require("../models");

module.exports = async (req, res, next) => {
  try {
    const { id } = req.params;
    const { status } = req.body;
    if (isNaN(parseInt(id))) {
      return next({ message: "order id should be a number" });
    }
    const order = await Order.findOne({ where: { id: +id }, raw: true });
    if (order === null) {
      return next({ message: "order not found" });
    } else if (order.status === "completed") {
      return res.status(400).json({ status: "fail", message: "order already completed, nothing changed" });
    }

    Order.update({ status }, { where: { id: +id } });
    return res.status(201).json({ status: "success", message: `data order with id ${id} updated` });
  } catch (error) {
    next(error);
  }
};
