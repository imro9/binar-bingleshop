const uploader = require('../libs/upload_cloudinary');
const item = require('../models').sequelize.model('item')
const logger = require('pino').default();
const acceptedFile = ['image/jpg', 'image/jpeg', 'image/png'];

const updateItem = async (req, res, next) => {
  try {
    const id = req.params.id; 
    const result = await uploader.upload(req.file.path)
    await item.update({
        img_url: result.secure_url,
    }, {
        where: {id},
    })
    res.sendStatus(204)
  } catch (error) {
    logger.error(error.message)
    next({status: 500, message: 'Error upload image'})    
  }
}

const itemImageValidator = (req, file, callback) => {
  if(!acceptedFile.includes(file.mimetype)) {
    logger.error(null, "Error uploading file. file type %s", file.mimetype)
    
    return callback(new Error('File must be .jpg|jpeg|png type'))
  }
  return callback(null, true)
}

module.exports = {
  updateItem, itemImageValidator
}