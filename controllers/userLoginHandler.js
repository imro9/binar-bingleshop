require("dotenv").config();
const User = require("../models").sequelize.model("user");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

module.exports = async (req, res, next) => {
  const { email, password } = req.body;
  const isFoundUser = await User.findOne({ raw: true, where: { email } });
  if (!isFoundUser) {
    res.status(400)
    res.send("User not found or not registered!");
    return next({message: "user not found"})
    
  } else if (isFoundUser.password !== password && !bcrypt.compareSync(password, isFoundUser.password)) {
    res.status(401)
    res.json({ status: "fail", message: "Password wrong!" });
    return next({message: "wrong password"})

  } else if (!isFoundUser.is_verified){
    res.status(400)
    res.json({ message: "account is not verified, please check your email!" });
    return next({message: "account is not verified"})

  } else {
    const { SECRET_KEY } = process.env;
    const accessToken = jwt.sign({ user_id: isFoundUser.id, role: isFoundUser.role }, SECRET_KEY);
    res.status(200)
    res.json({ message: "Login Success", token: `Bearer ${accessToken}` });
    return
  }
};
