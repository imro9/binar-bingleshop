require("dotenv").config();
const User = require("../models").sequelize.model("user");
const bcrypt = require("bcrypt");
const nodemailer = require('nodemailer');
const saltRounds = 10;


function sendEmail(targetUser, verification_code) {
  const { ADMIN_EMAIL, ADMIN_PASS } = process.env;
  let message = "";
  const transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 465,
    secure: true,
    auth: {
      user: ADMIN_EMAIL,
      pass: ADMIN_PASS,
    },
  });

  const mail = {
    from: ADMIN_EMAIL,
    to: targetUser,
    subject: "Verification Account",
    text: `Click this link for verify your account: http://localhost:3000/register/${verification_code} `,
  };

  transporter.sendMail(mail, (err, info) => {
    if (err) {
      console.log(err);
      message = "Error";
    } else {
      console.log("Email sent: " + info.response);
      message = "success";
    }
  });

  return message;
}


module.exports = async (req, res, next) => {
  const { name, email, password } = req.body;
  try {
    const user = await User.findOne({ where: { email }, raw: true });
    if (user == null) {
      const salt = bcrypt.genSaltSync(saltRounds);
      const hashPassword = bcrypt.hashSync(password, salt);
      const verification_code = Buffer.from(email).toString("base64url");
      const sendEmails = sendEmail(email, verification_code);
      if (sendEmails == "Error") {
        return res.status(500).json({ msg: "some internal error" });
      }
  
      await User.create({ name, email, password: hashPassword, verification_code}, {returning: false} );

      return res
        .status(200)
        .json({ status: "success", message: "register success, please check your email for verification!" });
    }
    return res
      .status(400)
      .json({ status: "fail", message: "email has been registered!" });
  } catch (error) {
    next(error);
  }
};
