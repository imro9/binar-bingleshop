const User = require("../models").sequelize.model("user");

module.exports = async (req, res) => {
    const { verif_code } = req.params;
    const userFound = await User.update({ verification_code: null, is_verified: true }, { where: { verification_code: verif_code } });
    if (userFound[0] == 0) {
      return res.status(400).json({ msg: "invalid link" });
    }
    return res.status(200).json({ msg: "Verification succes!" });
  }


