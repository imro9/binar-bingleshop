const express = require("express");
const routes = require("./routes");
const middleware = require("./middelware");
const socket = require("socket.io");

const socketController = require("./controllers/socket");

const app = express();

app.use(express.json());

app.use(routes);
app.get("/", (req, res) => res.sendStatus(200));
app.use(middleware.notFoundError);
app.use(middleware.internalError);

const server = app.listen(3000);

const io = socket(server);

socketController.setup(io);
