const multer = require("multer");
const upload_storage = require("./upload_storage");

module.exports = (validator) => multer({storage: upload_storage, fileFilter: validator})