require("dotenv").config();
const { SECRET_KEY } = process.env;
const passport = require("passport");
const { Strategy: JwtStrategy, ExtractJwt } = require("passport-jwt");

const options = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: SECRET_KEY,
};

passport.use(
  new JwtStrategy(options, (payload, done) => {
    done(null, { user_id: payload.user_id, role: payload.role });
  })
);

module.exports = passport.authenticate("jwt", { session: false });
