const logger = require('pino').default();
const fs = require('fs')

const cloudinary = require('cloudinary').v2;
cloudinary.config({
  cloud_name: 'dbpuoiz7x',
  api_key: '666937343381765',
  api_secret: 'chWieE60xeFq38XRzHo31NAj1YQ',
})

module.exports = {
  upload: async (filepath) => {
    
    try {
      const result = await cloudinary.uploader.upload(filepath, { folder: 'test/products/' })
      return result;
    } catch (error) {
      logger.error(error)
      throw new Error('Failed to upload image to cloudinary')
    } finally {
      fs.unlinkSync(filepath)
    }
  }
} 

