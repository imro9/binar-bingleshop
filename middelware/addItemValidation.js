const Validator = require("fastest-validator");
const v = new Validator();

module.exports = function (req, res, next) {
  const schema = {
    name: "string",
    category: "string",
    price: "number",
    stock: "number",
  };

  const check = v.compile(schema);
  const isValidate = check(req.body);
  if (isValidate !== true) {
    return res.status(400).send(isValidate);
  } else {
    return next();
  }
};
