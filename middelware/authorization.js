const passport = require("../libs/passport");

module.exports = (roles = []) => {
  return [
    passport,
    (req, res, next) => {
      if (roles.includes(req.user.role)) {
        return next();
      }
      res.status(403).json({ status: "fail", message: "You dont have access to this path!" });
    },
  ];
};
