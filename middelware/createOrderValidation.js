const Validator = require("fastest-validator");
const v = new Validator()

module.exports = (req, res, next) => {
  const schema = {
    data_item: { type: "array", items:{
      type: "object", props: {
        item_id: { type: "number", positive: true },
        quantity: { type: "number", positive: true}
      }
    }
  }
}
  const check = v.compile(schema);
  const isValidate = check(req.body);
  if(isValidate !== true){
    return res.status(400).json(isValidate)
  } else{
    return next()
  }
};
