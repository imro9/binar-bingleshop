const Validator = require("fastest-validator");
const v = new Validator();

module.exports = (req, res, next) => {
  const schema = {
    name: { type: "string", optional: true },
    category: { type: "string", optional: true },
    price: { type: "number", optional: true },
    stock: { type: "number", optional: true },
  };

  const check = v.compile(schema);
  const isValidate = check(req.body);
  if (isValidate !== true) {
    return res.status(400).json({ status: "fail", message: "incompatible data field sent" });
  }
  next();
};
