const Validator = require("fastest-validator");
const v = new Validator();

module.exports = (req, res, next) => {
  const schema = {
    status: { type: "enum", values: ["completed"] },
  };

  const check = v.compile(schema);
  const isValidate = check(req.body);
  if (isValidate !== true) {
    return res.status(400).json({ status: "fail", message: "incompatible data field sent" });
  }
  next();
};
