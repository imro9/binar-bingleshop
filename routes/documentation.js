const swaggerJSON = require("../resource/openapi.json");
const swaggerUI = require("swagger-ui-express");
const routeDocs = require("express").Router();

routeDocs.use("/apidoc", swaggerUI.serve, swaggerUI.setup(swaggerJSON));

module.exports = routeDocs;
