const routeDocs = require("./documentation");
const itemRoutes = require("./item");
const userRoutes = require("./user");
const orderRoutes = require("./order");
const routes = require("express").Router();

routes.use(itemRoutes);
routes.use(orderRoutes);
routes.use(routeDocs);
routes.use(userRoutes);

module.exports = routes;
