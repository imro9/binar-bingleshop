const handler = require("../controllers");
const middleware = require("../middelware");
const init_uploader = require("../libs/init_uploader");
const router = require("express").Router();
const uploader = init_uploader(handler.itemImageValidator);

router.get("/users/items", handler.findItemsByUserHandler);
router.get("/admin/items", middleware.authorization(["admin"]), handler.findItemsByAdmin);
router.post("/admin/items", middleware.authorization(["admin"]), middleware.addItemValidation, handler.addItemByAdmin);
router.put("/admin/items/:id", middleware.authorization(["admin"]), middleware.updateItemValidation, handler.updateItemByAdmin);
router.put("/admin/items/:id/image", middleware.authorization(["admin"]), uploader.single("image"), handler.updateItem);
router.delete("/admin/items/:id", middleware.authorization(["admin"]), handler.deleteItemsByAdmin);

module.exports = router;
