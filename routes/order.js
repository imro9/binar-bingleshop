const handler = require("../controllers");
const middleware = require("../middelware");
const router = require("express").Router();

router.put("/admin/order/:id", middleware.authorization(["admin"]), middleware.updateOrderValidation, handler.updateOrderByCostumer);
router.post("/users/order", middleware.authorization(["costumer"]), middleware.createOrderValidation, handler.createOrder);
router.delete("/users/order/:id", middleware.authorization(["costumer"]), handler.deleteOrderByCustomer);
router.get("/users/orders", middleware.authorization(["costumer"]), handler.findOrdersByUser);

module.exports = router;
