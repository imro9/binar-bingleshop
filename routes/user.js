const handler = require("../controllers");
const middleware = require("../middelware");
const router = require("express").Router();

router.post("/register", middleware.registerValidation, handler.userRegister);
router.get("/register/:verif_code", handler.verifAccountHandler);
router.post("/login", middleware.userLoginValidation, handler.userLoginHandler);
module.exports = router;
