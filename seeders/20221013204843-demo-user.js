"use strict";
const bcrypt = require("bcrypt");

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    const salt = bcrypt.genSaltSync(10);
    const hashedPassword = bcrypt.hashSync("binarian123", salt);
    await queryInterface.bulkInsert(
      "users",
      [
        { name: "Adrian", email: "adrian@gmail.com", password: hashedPassword },
        { name: "Jesika", email: "jesika@gmail.com", password: hashedPassword },
        { name: "Galang Gemilang", email: "gemilang@gmail.com", password: hashedPassword },
        { name: "Nadira", email: "nadira@gmail.com", password: hashedPassword },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete("users", null, {});
  },
};
