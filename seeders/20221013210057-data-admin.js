"use strict";
const bcrypt = require("bcrypt");

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    const salt = bcrypt.genSaltSync(10);
    const hashedPassword = bcrypt.hashSync("binarian123", salt);
    await queryInterface.bulkInsert(
      "users",
      [
        { name: "Admin 1", email: "admin1@gmail.com", password: hashedPassword, role: "admin", is_verified: true },
        { name: "Admin 2", email: "admin2@gmail.com", password: hashedPassword, role: "admin", is_verified: true },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete("users", null, {});
  },
};
